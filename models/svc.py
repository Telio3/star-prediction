import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score
from sklearn.svm import SVC


def SVC_model(x_train, y_train, x_test, y_test):
    model = SVC()
    model.fit(x_train, y_train)
    predicted = model.predict(x_test)
    conf = confusion_matrix(y_test, predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=model.classes_)
    disp.plot()
    plt.title("SVM")
    plt.show()
    print("The Accuracy of SVM is: ", accuracy_score(y_test, predicted) * 100)

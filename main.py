import pandas as pd
import plots
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from models.MLP_classifier import MLP_classifier_model
from models.logistic_regression import logistic_regression_model
from models.random_forest_classifier import random_forest_classifier_model
from models.svc import SVC_model

# get data
df = pd.read_csv("data/star.csv")

# print data
print(df.head())
print("-" * 50)
print(df.shape)
print("-" * 50)
print(df.isnull().sum())
print("-" * 50)
print(df.info())
print("-" * 50)

# plotting
plots.plot_all(df)

# normalize data
label_encoder = LabelEncoder()
df['Star color'] = label_encoder.fit_transform(df['Star color'])
df['Spectral Class'] = label_encoder.fit_transform(df['Spectral Class'])

x = df.drop("Star type", axis=1)
y = df["Star type"]

# split data into train and test
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=42)

# train models
logistic_regression_model(x_train, y_train, x_test, y_test)
SVC_model(x_train, y_train, x_test, y_test)
random_forest_classifier_model(x_train, y_train, x_test, y_test)
MLP_classifier_model(x_train, y_train, x_test, y_test)

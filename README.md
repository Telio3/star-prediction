# Prédiction du type d'étoile à partir de ses paramètres

CORRE Télio - DORE Mathis - HOLLANDE Youn - LEGRAND Yanis

## 1 - Introduction

Ce rapport a pour objectif d'examiner l'utilisation des algorithmes d'apprentissage en machine learning pour prédire le type d'étoile à partir de ses paramètres. Le jeu de données utilisé pour cette étude provient de kaggle.com. Il comporte 240 observations, avec 7 variables différentes. L'objectif de cette étude est de comprendre la pertinence des algorithmes sélectionnés pour la prédiction, ainsi que de déterminer les forces et les faiblesses de ces algorithmes.

![](resources/intro.jpg)

## 2 - Jeu de données

Le dataset utilisé dans cette étude est un ensemble de données de 240 étoiles. Chaque étoile est représentée par 7 paramètres :

- **La température** (en Kelvin)
- **La luminosité** (en L/Lo)
- **Le rayon** (en R/Ro)
- **La magnitude** absolue (en Mv)
- **La couleur** (Blanc, Red, Blue, Yellow, Yellow-Orange, Orange...)
- **La classe spectrale** (O, B, A, F, G, K, M)
- **Le type d'étoile** (0, 1, 2, 3, 4, 5)

| Star Type        | Star Classification   |
|------------------|-----------------------|
| 0                | Brown Dwarf           |
| 1                | Red Dwarf             |
| 2                | White Dwarf           |
| 3                | Main Sequence         |
| 4                | Supergiant            |
| 5                | Hypergiant            |


Les données ont été collectées à partir de [kaggle.com](https://www.kaggle.com/).

Le jeu de données est divisé en deux parties : un ensemble d'entraînement et un ensemble de test.

L'ensemble d'entraînement est utilisé pour entraîner les algorithmes d'apprentissage, tandis que l'ensemble de test est utilisé pour évaluer la précision des prédictions. Nous utilisons une répartition 2/3 pour diviser les données en ensemble d'entraînement et d'essai. Il faut donc prendre le temps de récupérer des données en quantité et qualité suffisantes, en évitant les biais de représentativité.

Suite à l'appel de ces méthodes :

```python
import pandas as pd

# get data
df = pd.read_csv("data/star.csv")

# print data
print(df.head())
print(df.shape)
print(df.isnull().sum())
print(df.info())
```

Nous obtenons les informations suivantes sur le jeu de données :

![](resources/dataset_info.png)

On peut  voir que le jeu de données ne contient pas de valeurs manquantes.

Mais, quand on affiche les types de données, on peut voir que les variables "Star color" et "Spectral Class" sont de type "object". Il faut donc les convertir pour pouvoir les utiliser dans les algorithmes d'apprentissage.

```python
# normalize data
label_encoder = LabelEncoder()
df['Star color'] = label_encoder.fit_transform(df['Star color'])
df['Spectral Class'] = label_encoder.fit_transform(df['Spectral Class'])
```

Le jeu de données est maintenant prêt à être utilisé pour l'apprentissage.

## 3 – Méthodologie

Dans cette étude, nous utiliserons plusieurs algorithmes d'apprentissage pour effectuer les prédictions. Les algorithmes choisis incluent :

- MLPClassifier : cet algorithme utilise un réseau de neurones multicouches pour effectuer la classification. Il peut traiter les relations complexes entre les différentes variables et est souvent utilisé pour les jeux de données dans lesquels les variables ont un impact significatif sur la variable cible.
- SVM (Support Vector Machine) : cet algorithme utilise une méthode de séparation pour faire la classification. Il est souvent utilisé pour les jeux de données dans lesquels les variables sont très corrélées.
- Régression logistique : cet algorithme effectue une régression en utilisant une fonction sigmoïde pour prédire la probabilité de l'occurrence d'un événement binaire. Il est souvent utilisé pour les problèmes de classification binaire.
- Random Forest : cet algorithme utilise une collection d'arbres de décision pour faire la classification. Il est souvent utilisé pour les jeux de données complexes avec de nombreuses variables.

Nous avons choisi ces algorithmes en raison de leur capacité à gérer les jeux de données complexes et leur capacité à faire des prédictions précises.

## 4 - Analyse des données

### I - Graphiques de données

![](resources/count_star_type.png)

On peut voir ici que le jeu de données est réparti de manière égale entre les différents types d'étoiles. Il n'y a donc pas de biais de représentativité.

![](resources/temperature_by_star_type.png)

Le graphique montre la température des étoiles en fonction de leur type.

On peut apercevoir que les étoiles de type "Brown Dwarf" et "Red Dwarf" sont les plus froides que les autres types d'étoiles.

![](resources/luminosity_by_star_type.png)

Le graphique montre la luminosité des étoiles en fonction de leur type.

On voit tout de suite que les étoiles de type "Super Giants" et "Hyper Giants" sont les plus lumineuses. Ceux sont elles aussi les plus chaudes.

![](resources/magnitude_by_star_type.png)

Le graphique montre la magnitude absolue des étoiles en fonction de leur type.

On observe ici que plus les étoiles ont une magnitude absolue élevée, plus les étoiles se rapprochent de la classe 0.

![](resources/radius_by_star_type.png)

Le graphique montre le rayon des étoiles en fonction de leur type.

Le seul type d'étoile qui a un rayon élevé est le type "Hyper Giant".
Avec tout de même une lègère exception pour le type "Super Giants".

![](resources/correlation_matrix.png)

La matrice de corrélation montre la corrélation entre les différentes variables.

On peut voir que la température, la luminosité et la magnitude absolue sont les variables les plus corrélées.


### II - Matrices de confusions

![](resources/SVM.png) ![](resources/random_forest.png) ![](resources/MLP.png) ![](resources/logistic_regression.png)

Voici les matrices de confusions de nos modèles utilisant nos différents algorithmes.

<u>Comment les lire ?</u> (cf Logistic Regression)

La matrice de confusion présente les résultats d'un modèle de régression logistique appliqué à un ensemble de données de test. L'ensemble de données comprend 6 classes, numérotées de 0 à 5.

Chaque colonne de la matrice représente la classe réelle de l'échantillon, tandis que chaque ligne représente la classe prédite par le modèle. Les valeurs des cellules de la matrice indiquent le nombre d'échantillons de la classe réelle correspondante qui ont été prédits dans la classe prédite correspondante.

En général, une matrice de confusion est utilisée pour évaluer la performance d'un modèle de classification. Une matrice de confusion bien remplie indique que le modèle est capable de prédire correctement les classes des échantillons.

Dans le cas de la matrice de confusion ci-dessous, le modèle semble avoir une performance globale satisfaisante.

Voici une analyse plus détaillée de la matrice de confusion :

- Pour la **classe réelle 0**, le modèle a correctement prédit 16 instances comme appartenant à la classe 0.
- Pour la **classe réelle 1**, le modèle a correctement prédit 12 instances comme appartenant à la classe 1, mais a fait une erreur en prédisant 1 instance comme appartenant à la classe 0.
- Pour la **classe réelle 2**, le modèle a correctement prédit 12 instances comme appartenant à la classe 2.
- Pour la **classe réelle 3**, le modèle a correctement prédit 10 instances comme appartenant à la classe 3, mais a fait une erreur en prédisant 1 instance comme appartenant à la classe 2.
- Pour la **classe réelle 4**, le modèle a correctement prédit 12 instances comme appartenant à la classe 4.
- Pour la **classe réelle 5**, le modèle a correctement prédit 16 instances comme appartenant à la classe 5.

En conclusion, la matrice de confusion ci-dessus indique que le modèle de régression logistique appliqué à cet ensemble de données de test a une performance globale satisfaisante. Le modèle est capable de prédire correctement les classes 0, 2, 3 et 4 avec un taux de 100%. En revanche, les classes 1 et 3 sont moins bien prédites mais restent correctement prédites dans la majorité des cas.

## 5 – Résultats

|          **Modèle**          | **Précision (%)** |
| :--------------------------: | :---------------: |
|      Logistic Regressor      |       97.50       |
| Support Vector Machine (SVM) |       28,75       |
|        MLPClassifier         |       36,25       |
|        Random Forest         |        100        |

Nous avons utilisé quatre algorithmes différents pour effectuer ces prédictions : la régression logistique (LR), le support vector machine (SVM), la forêt aléatoire (RF) et le réseau de neurones multicouches (NN).

L'analyse des données a révélé que l'algorithme de le forêt aléatoire a obtenu la plus grande précision de 100%, tandis que l'algorithme du support vector machine et du réseau de neurones multicouches ont obtenu la plus faible précision de 28,75% et 36,25% respectivement.
La régression logistique a terminé deuxième avec une précision de 97,5%.

## Ecart entre les modèles

L'écart entre les performances des différents modèles peut être expliqué par la nature des données et les hypothèses sous-jacentes à chaque modèle.

La régression logistique est un modèle linéaire qui suppose que la relation entre les paramètres et le type d'étoile est linéaire. Cette hypothèse est raisonnable pour les données utilisées dans cette étude, car les paramètres sont corrélés de manière linéaire.

Le SVM est un modèle non linéaire qui n'impose pas d'hypothèse sur la forme de la relation entre les paramètres et le type d'étoile. Cependant, le SVM est sensible au bruit dans les données. Les données utilisées dans cette étude contiennent un certain niveau de bruit, ce qui peut expliquer la faible précision du SVM.

La forêt aléatoire est un modèle non linéaire qui est moins sensible au bruit que le SVM. La forêt aléatoire combine les prédictions de plusieurs arbres de décision, ce qui permet de réduire le bruit dans les données.

Le MLP est un modèle non linéaire qui peut apprendre des relations complexes entre les paramètres et le type d'étoile. Cependant, le MLP est un modèle complexe qui peut être difficile à ajuster. Les données utilisées dans cette étude sont relativement petites, ce qui peut expliquer la faible précision du MLP.

## Conclusion

Les résultats de cette étude montrent que les modèles d'IA de prédiction peuvent être utilisés pour prédire le type d'étoile à partir de ses autres paramètres. Cependant, la précision des modèles dépend de la nature des données et des hypothèses sous-jacentes à chaque modèle.

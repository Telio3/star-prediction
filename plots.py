import seaborn as sns
import plotly.express as px
from matplotlib import pyplot as plt
from pandas import DataFrame


def plot_all(df: DataFrame):
    plot_star_type_count(df)
    plot_temperature_by_star_type(df)
    plot_luminosity_by_star_type(df)
    plot_magnitude_by_star_type(df)
    plot_radius_by_star_type(df)
    plot_correlation_matrix(df)
    # plot_count_star_type(df)
    # plot_star_type_vs_magnitude(df)


def plot_star_type_count(df: DataFrame):
    plt.figure(figsize=(12, 6))
    sns.countplot(y="Star type", data=df, palette='husl')
    plt.title('Count Plot: Star Type')
    plt.show()


def plot_temperature_by_star_type(df: DataFrame):
    plt.figure(figsize=(10, 6))
    sns.boxplot(x='Star type', y='Temperature (K)', data=df)
    plt.title('Temperature by Star Type')
    plt.show()


def plot_luminosity_by_star_type(df: DataFrame):
    plt.figure(figsize=(10, 6))
    sns.boxplot(x='Star type', y='Luminosity(L/Lo)', data=df)
    plt.title('Luminosity by Star Type')
    plt.show()


def plot_magnitude_by_star_type(df: DataFrame):
    plt.figure(figsize=(10, 6))
    sns.boxplot(x='Star type', y='Absolute magnitude(Mv)', data=df)
    plt.title('Magnitude by Star Type')
    plt.show()


def plot_radius_by_star_type(df: DataFrame):
    plt.figure(figsize=(10, 6))
    sns.boxplot(x='Star type', y='Radius(R/Ro)', data=df)
    plt.title('Radius by Star Type')
    plt.show()


def plot_correlation_matrix(df: DataFrame):
    correlation_matrix = df.corr()
    plt.figure(figsize=(12, 10))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
    plt.title('Correlation Matrix')
    plt.show()


def plot_count_star_type(df: DataFrame):
    fig = px.bar(df, y='Spectral Class', color="Star type", template='plotly_dark')
    fig.show()


def plot_star_type_vs_magnitude(df: DataFrame):
    fig = px.scatter(df,
                     x="Star type", y='Absolute magnitude(Mv)', size="Radius(R/Ro)", color="Temperature (K)",
                     log_x=True, size_max=60,
                     template='plotly_dark', title='Star Type vs Absolute Magnitude')
    fig.show()
